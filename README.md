Installation 
------------------
```
mkdir -p analysis/packages analysis/run
cd analysis/packages
setupATLAS
rcSetup Base,2.4.8
rc checkout_pkg atlasoff/PhysicsAnalysis/HiggsPhys/Run2/HGamma/xAOD/HGamAnalysisFramework/tags/HGamAnalysisFramework-00-02-47-01 
./HGamAnalysisFramework/scripts/setupRelease
git clone https://gitlab.cern.ch/jvasquez/HGamBtagStudies.git
rc find_packages 
rc compile
```

Running
-------------------
```
cd ~/analysis/run
runTaggingAnalysis HGamBtagStudies/TaggingAnalysis.cfg SampleName: ttH InputFileList: path/to/list_of_files.txt OutputDir: Btag_eff_ttH
```