#include "HGamBtagStudies/TaggingAnalysis.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TaggingAnalysis)



TaggingAnalysis::TaggingAnalysis(const char *name)
: HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



TaggingAnalysis::~TaggingAnalysis()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode TaggingAnalysis::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.
  
  TString WPstrings[4] = {"_60","_70","_77","_85"};
  TString tstrings[3] = {"_b","_c","_l"};
  TString ylabel = "; Tagging Efficiency";

  for( TString WPstr : WPstrings ) {
    for( TString tstr : tstrings ) {
      histoStore()->createTProfile("TagEff_pt" +tstr+WPstr, 150,   0.,  300, "; Jet p_{T} [GeV]"+ylabel );
      histoStore()->createTProfile("TagEff_eta"+tstr+WPstr,  60, -3.0,  3.0, "; Jet #eta"+ylabel );
    }
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TaggingAnalysis::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();
  
  // --------------------------------------------------------------
  // Object + Event Selection (Overlap Handled Correctly)
  // --------------------------------------------------------------
  //  Not totally necessary with MxAODs but needed for PRW currently
  
  // Photons 
  xAOD::PhotonContainer photons        = photonHandler()->getCorrectedContainer();
  xAOD::PhotonContainer preselphotons  = photonHandler()->applyPreSelection(photons);
  xAOD::PhotonContainer selphotons     = photonHandler()->applySelection(photons);

  // Electrons
  xAOD::ElectronContainer elecs        = electronHandler()->getCorrectedContainer();
  xAOD::ElectronContainer selels       = electronHandler()->applySelection(elecs);

  // Muons
  xAOD::MuonContainer muons            = muonHandler()->getCorrectedContainer();
  xAOD::MuonContainer selmus           = muonHandler()->applySelection(muons);

  // Jet Preselection 
  xAOD::JetContainer jets              = jetHandler()->getCorrectedContainer();
  xAOD::JetContainer seljets           = jetHandler()->applySelection(jets);

  // Overlap Removal
  overlapHandler()->removeOverlap(selphotons, seljets, selels, selmus);

  // MET Calculation
  xAOD::MissingETContainer met = etmissHandler()->getCorrectedContainer(&selphotons, &jets, &selels, &selmus);
  xAOD::MissingETContainer selmet = etmissHandler()->applySelection(met);

  // Require HGam Selection
  //  in MxAODs can also just use the isPassed variable
  if (!pass(&preselphotons, &selels, &selmus, &seljets)) return EL::StatusCode::SUCCESS; 
  setSelectedObjects(&selphotons, &selels, &selmus, &seljets, &selmet);


  // -------------------------------------------------
  // Loop over jets and plot efficiencies
  // -------------------------------------------------
  TString WPstrings[4] = {"_60","_70","_77","_85"};

  for( auto jet : seljets ) {
    if (fabs(jet->eta()) > 2.5) continue; // Only Central Jets
    //if ( jet->pt() > 300*HG::GeV) continue; // part of your selection, needed? 

    TString trType = "";
    int TruthLabel = fabs(jet->auxdata<int>("PartonTruthLabelID"));
    if      (TruthLabel == 5) trType = "_b";
    else if (TruthLabel == 4) trType = "_c";
    else if (TruthLabel <= 3) trType = "_l";
    else continue; 
    // maybe add gluons or taus?

    // Plot Efficiency for each Working Point
    for(TString WPstr : WPstrings) {
      TString wpName = "MV2c10_FixedCutBEff"+WPstr;
      bool PassTag = jet->auxdata<char>(wpName.Data());
      histoStore()->fillTProfile( "TagEff_pt"  + trType+WPstr, jet->pt()*1e-03, PassTag, weight() );
      histoStore()->fillTProfile( "TagEff_eta" + trType+WPstr, jet->eta(),      PassTag, weight() );
    }

  }


  return EL::StatusCode::SUCCESS;
}
